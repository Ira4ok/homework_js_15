/*SMOOTH SCROLL FROM ANCHOR LINK THROUGH THE DOCUMENT*/

//click on 'a' element within the parent block with id='nav-menu'
$('#nav-menu').on('click', 'a', function (e) {
    e.preventDefault();

    //obtain the identification of the clicked link
    let id = $(this).attr('href'),
      //get the distance in px from the top of the browser to the element
        top = $(id).offset().top;

    // console.log(id);
    // console.log(top);

    //make the animation on the page while moving from the top to the element
    $('html, body').animate({
                 scrollTop: top
            }, 1000, 'swing')
});


/*TOTOP BUTTON AND SCROLLING UP THE DOCUMENT*/
//on the event of window scroll more than 200px down the button appears, else - disappears
    $(window).scroll(function () {

        if($(this).scrollTop() > 200) {
        $('#to-top-btn').fadeIn()
    } else {
        $('#to-top-btn').fadeOut()
    }
});
    //on the event of btn click the page is animated to go to the very top.
        $('#to-top-btn').on('click', function () {

            $('html, body').animate({
                scrollTop: 0
            }, 1000);
        });


        /*TOGGLE THE SECTION WITH THE BUTTON*/

//while clicking on the slide button the relevant section closes or opens depending on its actual status
$('#slideBtn').on('click', function (event) {
    event.preventDefault();

    $('.popular-posts-section').slideToggle('slow');
});

